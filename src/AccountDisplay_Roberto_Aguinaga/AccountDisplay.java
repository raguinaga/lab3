/*
 * This application manipulates a collection of bank account objects.
 * These bank account objects are manipulated graphically.
 */

package AccountDisplay_Roberto_Aguinaga;

// Util imports
import java.util.ArrayList;

// Begin javafx imports
import javafx.application.Application;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;

public class AccountDisplay extends Application {

    @Override
    public void start(Stage stage) {
        // Create textfield, button, and label controls
        TextField textfield = new TextField("0.0");
        Button createAccountButton = new Button("Create Bank Account");
        Label accountLabel = new Label();

        // Add a tooltip to the textfield control
        textfield.setTooltip(new Tooltip("Enter a number here."));

        // Create an ArrayList of BankAccount objects
        ArrayList<BankAccount> accountList = new ArrayList<BankAccount>();

        // Create an event handler for the button
        createAccountButton.setOnAction(event -> {
            /*
            Add a new BankAccount to the ArrayList using the BankAccount
             constructor that takes a String argument.
             */
            accountList.add(new BankAccount(textfield.getText()));
            double lastBalance =
                    accountList.get(accountList.size()).getBalance();
            String labelString = String.format("The balance in the " +
                    "account is %.2f",lastBalance);
            accountLabel.setText(labelString);
        });
        // Create the hbox
        HBox hbox = new HBox(10,textfield,createAccountButton);
        HBox lowerBox = new HBox(accountLabel);

        // Vbox
        VBox vbox = new VBox(10, hbox, lowerBox);
        // Create a scene with the vbox as the root node
        Scene scene = new Scene(vbox);

        // Stage stuff
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
