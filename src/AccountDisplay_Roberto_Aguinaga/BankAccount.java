/*
 * This class describes a bank account.
 * As you can tell, I added some of my own style/preferences to the
 * code, as well as comments/documentation.
 */
package AccountDisplay_Roberto_Aguinaga;

public class BankAccount {
    // Field to hold a balance.
    private double balance = 0.0;

    /**
     * Default no-arg constructor. Sets the balane to zero.
     */
    public BankAccount() {
        balance = 0.0;
    }

    /**
     * Constructor that takes a double value as a parameter
     * @param balance The balance you want to initialize the account with
     */
    public BankAccount(double balance) {
        this.balance = balance;
    }

    /**
     * Constructor. Takes a string argument and parses it into a double
     * to initialize the balance.
     * @param str A string representing a starting balance.
     */
    public BankAccount(String str) {
        balance = Double.parseDouble(str);
    }

    /**
     * Allows you to add money to the bank account
     * @param amount The amount you want to add, as a double value.
     */
    public void deposit(double amount) {
        balance += amount;
    }

    /**
     * Allows you to add money to the bank account.
     * @param str A string representing the value you are adding to the
     * bank account.
     */
    public void deposit(String str) {
        balance += Double.parseDouble(str);
    }

    /**
     * Allows you to decrease the balance by taking money out.
     * @param amount A double value representing the money you are
     * taking out.
     */
    public void withdraw(double amount) {
        balance -= amount;
    }

    /**
     * Allows you to decrease the balance by taking money out.
     * @param str A string representation of the money you want to
     * withdraw.
     */
    public void withdraw(String str) {
        balance -= Double.parseDouble(str);
    }

    /**
     * Allows you set an entirely new balance for the bank account
     * @param balance The new balance as a numeric value
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /**
     * Allows you to set an entirely new balance for the bank account
     * @param str The new balance as a string
     */
    public void setBalance(String str) {
        balance = Double.parseDouble(str);
    }

    /**
     * Returns the current balance in the bank account.
     * @return The balance of the bank account as a numeric value.
     */
    public double getBalance() {
        return balance;
    }
}
